public class Random1 extends Solver {

	private final Random random = new Random(0);
	/**
	 * Constructs a new random SAT solver. You should change the string below
	 * from "random" to your ID. You should also change the name of this class.
	 * In Eclipse, you can do that easily by right-clicking on this file
	 * (RandomAgent.java) in the Package Explorer and choosing Refactor > Rename.
	 */
	public Random1() {
		super("random");
	}

	 @Override
	 public boolean solve(Assignment assignment) {
	        // If the problem has no variables, it is trivially true or false.
	        if (assignment.problem.variables.size() == 0) {
	            return assignment.getValue() == Value.TRUE;
	        }

	        // Repeat the GSAT algorithm until a satisfying assignment is found
	        for (int i = 0; i < 10000; i++) {
	            // Generate a random initial assignment
	            for (Variable variable : assignment.problem.variables) {
	                Value value = Value.values()[random.nextInt(2)];
	                assignment.setValue(variable, value);
	            }

	            // Keep trying until the assignment is satisfying.
	            while (assignment.getValue() != Value.TRUE) {
	                // Choose a variable whose value will be flipped.
	                Variable variable = chooseVariable(assignment);
	                Value value = assignment.getValue(variable);

	                // Assign the new value to the chosen variable.
	                assignment.setValue(variable, value);

	                // If the new assignment is satisfying, return true
	                if (assignment.getValue() == Value.TRUE) {
	                    return true;
	                }
	            }
	        }

	        // If the algorithm reaches the maximum number of tries, return false
	        return false;
	    }

	    /**
	     * Choose a variable to flip. With probability RANDOM_PROB, choose a
	     * variable at random. Otherwise, choose the variable which will
	     * minimize the number of clauses that are false after the flip.
	     *
	     * @param assignment the assignment being worked on
	     * @return a variable to flip
	     */
	    private Variable chooseVariable(Assignment assignment) {
	        // If we should choose a variable at random, do so
	        if (random.nextDouble() < 0.25) {
	            return assignment.problem.variables.get(random.nextInt(assignment.problem.variables.size()));
	        }

	        // Otherwise, find the variable which will minimize the number of false clauses after flipping
	        Variable bestVariable = null;
	        int bestScore = Integer.MAX_VALUE;
	        for (Variable variable : assignment.problem.variables) {
	        	Value value = assignment.getValue(variable);
	            int score = scoreVariable(variable, assignment, flip(value));
	            if (score < bestScore) {
	                bestVariable = variable;
	                bestScore = score;
	            }
	        }
	        return bestVariable;
	    }

	    /**
	     * Calculate the score of a variable. The score is the number of clauses
	     * that will be false if the variable is flipped.
	     *
	     * @param variable the variable to score
	     * @param assignment the assignment being worked on
	     * @return the score of the variable
	     */
	    private int scoreVariable(Variable variable, Assignment assignment, Value value) {
	        int score = 0;
	        for (Clause clause : variable.problem.clauses) {
	            if (!isSatisfied(assignment, clause)) {
	                score++;
	            }
	        }
	        return score;
	    }
	    
	    private boolean isSatisfied(Assignment assignment, Clause clause) {
			  for (Literal literal : clause.literals) {
			      Value value = assignment.getValue(literal.variable);
			      if ((literal.valence && value == Value.TRUE) ||
			          (literal.valence && value == Value.FALSE)) {
			        return true;
			      }
			    }
			    
			    return false;
			  }
	    private Value flip(Value value) {
	        if (value == Value.TRUE) {
	            return Value.FALSE;
	        } else if (value == Value.FALSE) {
	            return Value.TRUE;
	        } else {
	            return Value.UNKNOWN;
	        }
	    }
}
